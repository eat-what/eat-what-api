package server

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/eat-what/eat-what-api/controllers"
)

func NewRouter() *gin.Engine {
	router := gin.Default()
	router.Use(cors.Default())

	api := router.Group("api")
	{
		apiV01 := api.Group("v0.1")
		{
			apiV01.GET("decide-food", controllers.DecideFood)
			restaurants := apiV01.Group("nearby-restaurants")
			{
				restaurants.GET("", controllers.SearchNearbyRestaurants)
				restaurants.GET("/:id", controllers.GetRestaurant)
			}
		}
	}

	return router
}
