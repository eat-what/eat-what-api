package server

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/eat-what/eat-what-api/config"
	"log"
	"net/http"
	"time"
)

type Server struct {
	server *http.Server
	ctx    context.Context
}

func Init() *Server {
	conf := config.GetConfig()
	// set deployment mode
	if conf.Debug {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}
	// hook up http server with gin router
	httpServer := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", conf.Host, conf.Port),
		Handler: NewRouter(),
	}

	return &Server{
		server: httpServer,
		ctx:    context.Background(),
	}
}

func (s *Server) Start() {
	go func() {
		if err := s.server.ListenAndServe(); err != nil {
			log.Fatalf("server failed to listen: %s\n", err)
		}
	}()
}

func (s *Server) Shutdown() {
	ctx, cancel := context.WithTimeout(s.ctx, 5*time.Second)
	defer cancel()

	if err := s.server.Shutdown(ctx); err != nil {
		log.Fatal("server shutdown:", err)
	}
	log.Println("server exited.")
}
