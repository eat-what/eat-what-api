package utils

import (
	"bytes"
	"strconv"
)

func Uint8ArrayToString(numbers []uint8) string {
	buffer := bytes.NewBufferString("")
	for i, number := range numbers {
		buffer.WriteString(strconv.Itoa(int(number)))
		if i < len(numbers)-1 {
			buffer.WriteString(", ")
		}
	}

	return buffer.String()
}
