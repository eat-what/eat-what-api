package clients

import (
	"gitlab.com/eat-what/eat-what-api/common"
	"gitlab.com/eat-what/eat-what-api/common/clients/yelp"
	"gitlab.com/eat-what/eat-what-api/config"
	"sync"
)

type (
	ClientPool map[common.ClientType]chan common.RestaurantFinder
)

var (
	initialize sync.Once
	pool       ClientPool
)

func Init() {
	initialize.Do(func() {
		conf := config.GetConfig()
		yelpConfig, ok := conf.ClientsConfig[common.YelpClient].(*yelp.Config)
		if !ok {
			panic("cannot find configuration for yelp")
		}
		poolMap := make(map[common.ClientType]chan common.RestaurantFinder, common.UnknownClient)
		for clientType := common.ClientType(0); clientType < common.UnknownClient; clientType++ {
			poolMap[clientType] = make(chan common.RestaurantFinder, conf.MaxAPIConnection)
			// pre-creation of api clients
			for i := uint64(0); i < conf.MaxAPIConnection; i++ {
				var finder common.RestaurantFinder
				switch common.ClientType(clientType) {
				case common.YelpClient:
					finder = yelp.New(yelpConfig)
				case common.DazhongClient:
					continue
				default:
					panic("unknown type of finder")
				}
				poolMap[clientType] <- finder
			}
		}

		pool = ClientPool(poolMap)
	})
}

func GetClientByType(clientType common.ClientType) common.RestaurantFinder {
	return <-pool[clientType]
}

func GetClientByLocale(locale string) (common.ClientType, common.RestaurantFinder) {
	switch locale {
	case "", "en_US":
		return common.YelpClient, GetClientByType(common.YelpClient)
	default:
		return common.UnknownClient, nil
	}
}

func ReturnClient(finder common.RestaurantFinder, clientType common.ClientType) {
	pool[clientType] <- finder
}
