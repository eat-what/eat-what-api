package yelp

import (
	"bytes"
	"context"
	"fmt"
	"github.com/json-iterator/go"
	"gitlab.com/eat-what/eat-what-api/common"
	"gitlab.com/eat-what/eat-what-api/common/utils"
	"net/http"
	"strings"
	"time"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

const (
	apiURL            = "https://api.yelp.com/v3/graphql"
	defaultLocale     = "en_US"
	defaultSearchTerm = "restaurant"

	requestTypeHeader  = "application/graphql"
	responseTypeHeader = "application/json"
)

type Client struct {
	cancel context.CancelFunc
	ctx    context.Context
	client http.Client
	config *Config
}

func New(config *Config) *Client {
	ctx, cancel := context.WithCancel(context.Background())
	return &Client{
		client: http.Client{
			Timeout: time.Duration(5) * time.Second,
		},
		cancel: cancel,
		ctx:    ctx,
		config: config,
	}
}

func (c *Client) GetNearbyRestaurants(commute string, long, lat float64, locale string, category *common.Category, review bool) (*common.SearchResult, error) {
	query := c.buildSearchQuery(commute, long, lat, locale, category, review)
	var result graphQLSearchResult
	err := c.sendQuery(query, &result)
	if err != nil {
		return nil, err
	}
	return c.parseSearchResult(&result), nil
}

func (c *Client) GetRestaurantDetail(id string) (*common.Restaurant, error) {
	query := c.buildDetailQuery(id)
	var result graphQLDetailResult
	err := c.sendQuery(query, &result)
	if err != nil {
		return nil, err
	}
	return c.parseDetailResult(&result), nil
}

func (c *Client) sendQuery(query *string, result interface{}) error {
	// build http request
	req, err := http.NewRequest(http.MethodPost, apiURL, bytes.NewBuffer([]byte(*query)))
	if err != nil {
		return err
	}
	// setup request header
	c.setRequestHeader(req)
	// send out the request
	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	// decode result
	dec := json.NewDecoder(resp.Body)
	dec.DisallowUnknownFields()
	err = dec.Decode(result)
	if err != nil {
		return common.ErrRemoteAPI
	}
	return nil
}

func (c *Client) buildSearchQuery(commute string, long, lat float64, locale string, category *common.Category, review bool) *string {
	var term, price string
	if category != nil {
		term = fmt.Sprintf("%s %s %s", category.Region, category.Flavour, category.Dish)
		price = utils.Uint8ArrayToString(category.Price)
	} else {
		term = defaultSearchTerm
		price = common.DefaultPriceRange
	}
	if locale == "" {
		locale = defaultLocale
	}
	restaurantDetail := c.resultFields(review)
	searchQuery := fmt.Sprintf(baseSearchQuery, term, lat, long, common.CommuteMap[commute], locale, price, *restaurantDetail)
	return &searchQuery
}

func (c *Client) buildDetailQuery(id string) *string {
	query := fmt.Sprintf(baseBusinessQuery, id, *c.resultFields(true))
	return &query
}

func (c *Client) resultFields(detail bool) *string {
	var restaurantDetail string
	if detail {
		restaurantDetail = fmt.Sprintf(businessFields, detailQuery)
	} else {
		restaurantDetail = fmt.Sprintf(businessFields, "")
	}
	return &restaurantDetail
}

func (c *Client) setRequestHeader(req *http.Request) {
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.config.APIKey))
	req.Header.Set("Content-Type", requestTypeHeader)
	req.Header.Set("Accept", responseTypeHeader)
}

func (c *Client) parseSearchResult(result *graphQLSearchResult) *common.SearchResult {
	response := common.SearchResult{
		Total:       result.Data.Search.Total,
		Restaurants: make([]common.Restaurant, len(result.Data.Search.Restaurants)),
	}
	for i, restaurant := range result.Data.Search.Restaurants {
		r := &response.Restaurants[i]
		address := strings.Split(restaurant.Location.FormattedAddress, "\n")
		r.Location = strings.Join(address, ", ")
		r.ID = restaurant.ID
		r.Name = restaurant.Name
		r.Rating = restaurant.Rating
		r.ReviewCount = restaurant.ReviewCount
		r.Photos = restaurant.Photos
		if restaurant.Reviews != nil {
			r.Reviews = make([]common.Review, len(restaurant.Reviews))
			for j, review := range restaurant.Reviews {
				c := &r.Reviews[j]
				c.Rating = review.Rating
				c.Link = review.URL
				c.Text = review.Text
				c.TimeCreated = review.TimeCreated
			}
		}
	}
	return &response
}

func (c *Client) parseDetailResult(result *graphQLDetailResult) *common.Restaurant {
	var response common.Restaurant
	r := result.Data.Restaurant
	address := strings.Split(r.Location.FormattedAddress, "\n")
	response.ID = r.ID
	response.Location = strings.Join(address, ", ")
	response.Rating = r.Rating
	response.ReviewCount = r.ReviewCount
	response.Name = r.Name
	response.Reviews = make([]common.Review, len(r.Reviews))
	response.Photos = r.Photos
	for i, review := range r.Reviews {
		rv := &response.Reviews[i]
		rv.Rating = review.Rating
		rv.Link = review.URL
		rv.Text = review.Text
		rv.TimeCreated = review.TimeCreated
	}
	return &response
}
