package yelp

const (
	baseSearchQuery = `{
		search(term: "%s",
			latitude: %f,
			longitude: %f,
			radius: %d,
			locale: "%s",
			price: "%s",
			sort_by: "best_match",
			open_now: true,
			limit: 20) {
			total
			business {
				%s
			}
		}
	}`

	baseBusinessQuery = `{
		business(id: "%s") {
			%s
		}
	}`

	businessFields = `
		id
		name
		rating
		review_count
		photos
		location {
			formatted_address
		}
		%s`

	detailQuery = `
		reviews {
			text
			rating
			time_created
			url
		}`
)
