package yelp

type graphQLSearchResult struct {
	Data searchData `json:"data"`
}

type graphQLDetailResult struct {
	Data restaurantData `json:"data"`
}

type restaurantData struct {
	Restaurant restaurant `json:"business"`
}

type searchData struct {
	Search searchResult `json:"search"`
}

type searchResult struct {
	Total       uint64       `json:"total"`
	Restaurants []restaurant `json:"business"`
}

type restaurant struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	Rating      float32  `json:"rating"`
	ReviewCount uint64   `json:"review_count"`
	Reviews     []review `json:"reviews,omitempty"`
	Photos      []string `json:"photos"`
	Location    struct {
		FormattedAddress string `json:"formatted_address"`
	} `json:"location"`
}

type review struct {
	Text        string `json:"text"`
	Rating      uint8  `json:"rating"`
	TimeCreated string `json:"time_created"`
	URL         string `json:"url"`
}
