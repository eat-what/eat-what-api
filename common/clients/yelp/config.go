package yelp

type Config struct {
	ClientID string
	APIKey   string
}
