package common

type Commute string

type ClientType uint8

const (
	YelpClient ClientType = iota
	DazhongClient
	UnknownClient
)

// commute distance in meters
const (
	walking uint64 = 1500
	cycling        = 5000
	driving        = 15000
)

const DefaultPriceRange string = "1, 2, 3, 4"

var CommuteMap = map[string]uint64{
	"walking": walking,
	"cycling": cycling,
	"driving": driving,
}

type Category struct {
	Region  string
	Flavour string
	Dish    string
	Price   []uint8 // price level, something like [1, 2, 3, 4], corresponding to restaurants with price level $, $$, $$$, $$$$
}

type SearchResult struct {
	Total       uint64       `json:"total"`
	Restaurants []Restaurant `json:"restaurants"`
}

type Restaurant struct {
	ID          string   `json:"id"`
	Name        string   `json:"name"`
	Location    string   `json:"location"`
	Rating      float32  `json:"rating"`
	ReviewCount uint64   `json:"reviewCount"`
	Reviews     []Review `json:"reviews,omitempty"`
	Photos      []string `json:"photos"`
}

type Review struct {
	Text        string `json:"text"`
	Rating      uint8  `json:"rating"`
	TimeCreated string `json:"timeCreated"`
	Link        string `json:"link"`
}

type RestaurantFinder interface {
	GetNearbyRestaurants(commute string, long, lat float64, locale string, category *Category, review bool) (*SearchResult, error)
	GetRestaurantDetail(id string) (*Restaurant, error)
}
