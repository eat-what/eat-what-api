package common

import "github.com/pkg/errors"

var (
	ErrRemoteAPI = errors.New("remote api error")
)
