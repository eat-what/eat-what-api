# 吃啥API

## 如何build

```bash
eat-what@mac: make build
```

## APIs(v0.1) `/api/v0.1`

#### **Decide Food**
____

Returns json data of a random selected restaurant.

* **URL**

  `/decide-food`

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `commute=[string] (walking/cycling/driving)`
   
   `long=[float]`
   
   `lat=[float]`
   
   **Optional:**
   
   `category=[not implemented]`
   
   `locale=[string] default="en_US"`
   

* **Data Params**

  `None`

* **Success Response:**

  * **Code:** `200` <br />
    **Content:** 
    
     ```json
     {
      "result": {
        "id": "bNQVXqWpsToBDcJg_4aWYA",
        "name": "Green Grotto",
        "location": "7040 Warden Avenue, Markham, ON L3R 5Y3, Canada",
        "rating": 3.5,
        "reviewCount": 138
      },
      "status": "success"
    }
    ```
 
* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    
    ```json
    { "status": "fail", "reason": "error" }
    ```

  OR

  * **Code:** 500 INTERNAL SERVER ERROR <br />
    **Content:** 
    
    ```json
    { "status": "fail", "reason": "error" }
    ```

* **Sample Call:**

  ```bash
  curl "http://eat-what-api-prod.herokuapp.com/api/v0.1/decide-food?long=-79.327766&lat=43.827942&commute=walking"
  ```

#### **Nearby Restaurants**
____

Returns json data of 20 nearby restaurants with reviews.

* **URL**

  `/decide-food`

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `commute=[string] (walking/cycling/driving)`
   
   `long=[float]`
   
   `lat=[float]`
   
   **Optional:**
   
   `category=[not implemented]`
   
   `locale=[string] default="en_US"`
   

* **Data Params**

  `None`

* **Success Response:**

  * **Code:** `200` <br />
    **Content:** 
    
     ```json
     {
      "result": {
        "total": 1,
        "restaurants": [{
          "id": "bNQVXqWpsToBDcJg_4aWYA",
          "name": "Green Grotto",
          "location": "7040 Warden Avenue, Markham, ON L3R 5Y3, Canada",
          "rating": 3.5,
          "reviewCount": 138,
          "reviews": [{
            "text": "Pros:\nVery rich broth\nUnlimited noodles\n\nCons:\nBowls are a hazard because they are so hot\n\nBecause one of my little one's allergies, anything rice noodle...",
            "rating": 4,
            "timeCreated": "2018-09-20 04:44:46",
            "link": "https://www.yelp.com/biz/dagu-rice-noodle-markham-markham-2?hrid=PLBICKoem6KCC94WIXj8Nw&adjust_creative=xKeLtAIga17kPm_HfhP2aA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=xKeLtAIga17kPm_HfhP2aA"
          }]
        }]
      },
      "status": "success"
    }
    ```
 
* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    
    ```json
    { "status": "fail", "reason": "error" }
    
    ```

  OR

  * **Code:** 500 INTERNAL SERVER ERROR <br />
    **Content:**
    
    ```json
    { "status": "fail", "reason": "error" }
    ```

* **Sample Call:**

  ```bash
  curl "http://eat-what-api-prod.herokuapp.com/api/v0.1/nearby-restaurants?long=-79.327766&lat=43.827942&commute=walking"
  ```

#### **Restaurant Detail**
____

Returns json data of a specified restaurant.

* **URL**

  `/nearby-restaurants/:id`

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `None`
   
   **Optional:**
   
   `None`
   

* **Data Params**

  `None`

* **Success Response:**

  * **Code:** `200` <br />
    **Content:** 
    
     ```json
     {
      "result": {
        "id": "EKnbQlntPX0FsgAC94XniA",
        "name": "Dagu Rice Noodle Markham",
        "location": "20 Gibson Drive, Unit 111A, Markham, ON L3R 4N3, Canada",
        "rating": 3,
        "reviewCount": 74,
        "reviews": [{
          "text": "Pros:\nVery rich broth\nUnlimited noodles\n\nCons:\nBowls are a hazard because they are so hot\n\nBecause one of my little one's allergies, anything rice noodle...",
          "rating": 4,
          "timeCreated": "2018-09-20 04:44:46",
          "link": "https://www.yelp.com/biz/dagu-rice-noodle-markham-markham-2?hrid=PLBICKoem6KCC94WIXj8Nw&adjust_creative=xKeLtAIga17kPm_HfhP2aA&utm_campaign=yelp_api_v3&utm_medium=api_v3_graphql&utm_source=xKeLtAIga17kPm_HfhP2aA"
        }]
      },
      "status": "success"
    }
    ```
 
* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    
    ```json
    { "status": "fail", "reason": "error" }
    ```

  OR

  * **Code:** 500 INTERNAL SERVER ERROR <br />
    **Content:** 
    
    ```json
    { "status": "fail", "reason": "error" }
    ```

* **Sample Call:**

  ```bash
  curl "http://eat-what-api-prod.herokuapp.com/api/v0.1/nearby-restaurants/EKnbQlntPX0FsgAC94XniA"
  ```
