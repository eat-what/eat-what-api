PACKAGE_NAME=gitlab.com/eat-what/eat-what-api
NAME=eat-what-api
VERSION=0.0.1
JSON_TAG := -tags=jsoniter
GO_BUILD_ENV := CGO_ENABLED=0 GOOS=linux GOARCH=amd64
DOCKER_BUILD=$(shell pwd)/.docker_build
DOCKER_CMD=$(DOCKER_BUILD)/eat-what-api
GO_BIN=$(GOPATH)/bin

$(DOCKER_CMD): clean
	mkdir -p $(DOCKER_BUILD)
	$(GO_BUILD_ENV) go build -v $(JSON_TAG) -o $(DOCKER_CMD) .

.PHONY: build
build:
	@go build $(JSON_TAG) -o $(NAME) .
	@chmod +x $(NAME)

.PHONY: install
install: build
	@mv $(NAME) $(GO_BIN)

.PHONY: run
run: build
	@./$(NAME) -e development

heroku: $(DOCKER_CMD)
	heroku container:push web

.PHONY: run-prod
run-prod: build
	@./$(NAME) -e production

.PHONY: clean
clean:
	@rm -f $(NAME)
	@rm -rf $(DOCKER_BUILD)

.PHONY: test
test:
	@go test -v ./tests/*
