package main

import (
	"flag"
	"fmt"
	_ "github.com/heroku/x/hmetrics/onload"
	"gitlab.com/eat-what/eat-what-api/common/clients"
	"gitlab.com/eat-what/eat-what-api/config"
	"gitlab.com/eat-what/eat-what-api/server"
	"os"
	"os/signal"
	"runtime"
)

func setupFlags() {
	debug := flag.Bool("debug", false, "Run API server in debug mode")
	yelpAPIKey := flag.String("yelp-api-key", "", "Yelp API Key to be used")
	yelpClientID := flag.String("yelp-client-id", "", "Yelp Client ID to be used")
	flag.Usage = func() {
		fmt.Println(
			"Usage: eat-what-api\n",
			"-debug [optional: enable debug mode]\n",
			"-yelp-api-key YOUR_YELP_API_KEY [optional: set yelp api key via command flag]\n",
			"-yelp-client-id YOUR_YELP_CLIENT_ID [optional: set yelp client id via command flag]",
		)
		os.Exit(1)
	}
	flag.Parse()
	if *debug {
		os.Setenv("DEBUG_MODE", "true")
	}
	if *yelpAPIKey != "" {
		os.Setenv("YELP_API_KEY", *yelpAPIKey)
	}
	if *yelpClientID != "" {
		os.Setenv("YELP_CLIENT_ID", *yelpClientID)
	}
}

func main() {
	// setup go runtime cpu cores
	runtime.GOMAXPROCS(runtime.NumCPU())
	// setup system signal detection for graceful exit
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, os.Kill)
	// setup command line flags
	setupFlags()
	// load configurations
	config.Init()
	// initialize api clients
	clients.Init()
	// initialize server
	svr := server.Init()
	// start the server
	svr.Start()
	// waiting for exit signal here
	<-quit
	// graceful exit
	svr.Shutdown()
}
