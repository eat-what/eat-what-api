package config

import (
	"gitlab.com/eat-what/eat-what-api/common"
	"gitlab.com/eat-what/eat-what-api/common/clients/yelp"
	"os"
	"strconv"
)

type Config struct {
	Host             string
	Port             uint16
	Debug            bool
	MaxAPIConnection uint64
	ClientsConfig    map[common.ClientType]interface{}
}

var config *Config

func Init() {
	if _, ok := os.LookupEnv("DEBUG_MODE"); ok {
		config = &DefaultDevConfig
	} else {
		config = &DefaultProdConfig
	}
	if portArg, ok := os.LookupEnv("PORT"); ok {
		port, err := strconv.Atoi(portArg)
		if err != nil {
			os.Exit(1)
		}
		config.Port = uint16(port)
	}
	config.ClientsConfig = map[common.ClientType]interface{}{
		common.YelpClient: &yelp.Config{
			APIKey:   os.Getenv("YELP_API_KEY"),
			ClientID: os.Getenv("YELP_CLIENT_ID"),
		},
	}
}

func GetConfig() *Config {
	return config
}
