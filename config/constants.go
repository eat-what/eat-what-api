package config

var (
	DefaultDevConfig = Config{
		Host:             "127.0.0.1",
		Port:             9211,
		Debug:            true,
		MaxAPIConnection: 10,
	}
	DefaultProdConfig = Config{
		Host:             "0.0.0.0",
		Port:             9245,
		Debug:            false,
		MaxAPIConnection: 100,
	}
)
