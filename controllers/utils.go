package controllers

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/eat-what/eat-what-api/common"
	"strconv"
)

func parseSearchQuery(c *gin.Context) (commute string, long, lat float64, category *common.Category, locale string, err error) {
	var ok bool
	locale = c.Query("locale")
	if locale != "" && locale != "en_US" {
		err = errors.New("currently we don't support your region")
		return
	}
	latQuery, ok := c.GetQuery("lat")
	if !ok {
		err = errors.New("missing latitude")
		return
	}
	lat, err = strconv.ParseFloat(latQuery, 64)
	if err != nil {
		err = fmt.Errorf("%s is not a valid coordinate", latQuery)
		return
	}
	longQuery, ok := c.GetQuery("long")
	if !ok {
		err = errors.New("missing longitude")
		return
	}
	long, err = strconv.ParseFloat(longQuery, 64)
	if err != nil {
		err = fmt.Errorf("%s is not a valid coordinate", longQuery)
		return
	}
	commute, ok = c.GetQuery("commute")
	if !ok {
		err = errors.New("missing commute type")
	}
	return
}
