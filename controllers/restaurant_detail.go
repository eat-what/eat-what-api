package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/eat-what/eat-what-api/common"
	"gitlab.com/eat-what/eat-what-api/common/clients"
	"net/http"
)

// API for retrieving nearby restaurants based on filter and location
// usage: GET /api/v0.1/nearby-restaurants?long=float&lat=float&commute=walking/driving/cycling

func GetRestaurant(c *gin.Context) {
	id := c.Param("id")
	if id == "" {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "fail",
			"reason": "please provide the id of restaurant you need to query",
		})
		return
	}

	locale := c.Query("locale")
	if locale != "" && locale != "en_US" {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "fail",
			"reason": fmt.Sprintf("unsupported locale %s", locale),
		})
		return
	}

	clientType, client := clients.GetClientByLocale(locale)
	defer clients.ReturnClient(client, clientType)

	if clientType == common.UnknownClient {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "fail",
			"reason": fmt.Sprintf("unsupported locale %s", locale),
		})
		return
	}

	result, err := client.GetRestaurantDetail(id)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"status": "fail",
			"reason": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status": "success",
		"result": result,
	})
}
