package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/eat-what/eat-what-api/common"
	"gitlab.com/eat-what/eat-what-api/common/clients"
	"net/http"
)

// API for retrieving nearby restaurants based on filter and location
// usage: GET /api/v0.1/nearby-restaurants?long=float&lat=float&commute=walking/driving/cycling

func SearchNearbyRestaurants(c *gin.Context) {
	commute, long, lat, category, locale, err := parseSearchQuery(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "fail",
			"reason": err.Error(),
		})
		return
	}

	clientType, client := clients.GetClientByLocale(locale)
	defer clients.ReturnClient(client, clientType)

	if clientType == common.UnknownClient {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "fail",
			"reason": fmt.Sprintf("unsupported locale %s", locale),
		})
		return
	}

	result, err := client.GetNearbyRestaurants(commute, long, lat, locale, category, true)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"status": "fail",
			"reason": err.Error(),
		})
		return
	} else if len(result.Restaurants) == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"status": "fail",
			"result": "unable to find any restaurant near you",
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"status": "success",
		"result": result,
	})
}
