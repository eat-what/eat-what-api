package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/eat-what/eat-what-api/common"
	"gitlab.com/eat-what/eat-what-api/common/clients"
	"math/rand"
	"net/http"
)

func DecideFood(c *gin.Context) {
	commute, long, lat, category, locale, err := parseSearchQuery(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "fail",
			"reason": err.Error(),
		})
		return
	}

	clientType, client := clients.GetClientByLocale(locale)
	defer clients.ReturnClient(client, clientType)

	if clientType == common.UnknownClient {
		c.JSON(http.StatusBadRequest, gin.H{
			"status": "fail",
			"reason": fmt.Sprintf("unsupported locale %s", locale),
		})
		return
	}

	result, err := client.GetNearbyRestaurants(commute, long, lat, locale, category, false)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"status": "fail",
			"reason": err.Error(),
		})
		return
	}

	numRestaurants := len(result.Restaurants)
	if numRestaurants == 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"status": "fail",
			"result": "unable to find any restaurant near you",
		})
		return
	}

	godsChoice := rand.Intn(numRestaurants)
	c.JSON(http.StatusOK, gin.H{
		"status": "success",
		"result": &result.Restaurants[godsChoice],
	})
}
